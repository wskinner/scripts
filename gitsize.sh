#!/bin/bash
# Given a directory as argument, write a list of all git directories
# that are in the directory's subtree, sorted by size.
sizes=''
find $1 -name '.git' -type d > allgits.txt
for f in $(cat allgits.txt)
do
	size=$(du -ck $f | grep total | cut -f1)
	echo "$size $f" >> all
done
uniq all| sort -n -k1,1
rm all allgits.txt
